// 1. Екранування - це "\" і він створений що б записувать символи, котрі не можуть виводитись в строці, через цей символ.
// 2. Оголошувати фунції можно через function declaration, через змінну, через конструктор, через метод, hoisting
// 3. hoisting працює для змінних так - якщо в функції після консоль лог оголосити змінну, котру ми викликаємо - виведеться помилка, 
// але якщо оголосити через var то змінна виведеться нормально, так само працює з функціями якщо просто оголосити
// функцію після консоль лог через function declaration то все буде працювать нормально, але оголосити функцію через function expression то буде помилка



/////////////////////////////////////////////////////////////
function createNewUser() {
    const newUser = {
        getLogin: function() {
            return (this.firstName.charAt(0).toLowerCase() + this.lastName.toLowerCase());
    },
    setNewName: function (name) {
        Object.defineProperty(this, 'firstName', {
           value: name,
            });
     },
     setLastName: function (surname) {
        Object.defineProperty(this, 'lastName', {
            value: surname
        })
     },
     birthday: prompt('Your birtday?', 'dd.mm.yyyy'),
     getPassword: function() {
        return this.firstName.slice(0, 1).toUpperCase() + this.lastName.toLowerCase() + this.birthday.substr(-4, 4);
     },
     getAge: function () {
        let age = new Date();
     let normalizeAge = new Date();
     normalizeAge.setFullYear(this.birthday.substr(-4, 4), this.birthday.substr(3, 2) - 1, this.birthday.substr(0, 2))
     age = age - normalizeAge;
     return Math.floor(age / (1000 * 60 * 60 * 24 * 30 * 12));
     }
    };
   
    Object.defineProperties(newUser, {
        firstName: {
             value: prompt('Your name?'), 
             writable: false,
             configurable: true 
            },
        lastName: { 
            value: prompt('Your surname?'), 
            writable: false, 
            configurable: true 
        },
      });
      newUser.birthday = prompt('Your birtday?', 'dd.mm.yyyy');
     
    return newUser;

}
let myUser = createNewUser();
console.log(myUser.getAge());
console.log(myUser.getPassword())
